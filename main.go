package main

import (
	"math/rand"
	"fmt"
	"time"
)


func randomNumber(minimo, maximo int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return minimo + rand.Intn(maximo - minimo)
}

func main() {
	var numero, acumulador, limite int
	var primosGenerados []int

	maximo := 100
	minimo := 1
	
	fmt.Println("Escrbe el valor a generar:")
	fmt.Scanf("%d", &numero)


	for i := 1; i <= numero; i++ {
		valorRandom := randomNumber(minimo, maximo)
		var numeroDivisores []int

		fmt.Println()
		fmt.Println("Numero Generado:\t", valorRandom)
		fmt.Print("Numero de divisores:\t")

		for div := 1; div <= valorRandom; div++ {
			if valorRandom % div == 0 {
				numeroDivisores = append(numeroDivisores, div)
				acumulador++
				limite++

				if limite == 10 {
					fmt.Printf("\n\t\t\t")
					limite = 0
				}
			}
			
		}
		fmt.Println(numeroDivisores)

		if acumulador == 2 {
			fmt.Println("Cantidad de divisores:", acumulador)
			fmt.Println("Si es primo")
			primosGenerados = append(primosGenerados, valorRandom)
		} else {
			fmt.Println("Cantidad de divisores:", acumulador)
			fmt.Println("No es primo")
		}

	}

	fmt.Println("-------------------------------")
	fmt.Printf("Los números primos generados fueron %d\n", primosGenerados)

}