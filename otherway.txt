package main

import (
	"fmt"
)

func main() {
	var numero, acumulador, limite int
	var k []int
	
	fmt.Println("Escrbe un número:")
	fmt.Scanf("%d", &numero)

	fmt.Println()
    fmt.Println("Numero ingresado:\t", numero)
    fmt.Print("Numero de divisores:\t")

	for div := 1; div <= numero; div++ {
		if numero % div == 0 {
			k = append(k, div)
			acumulador++
			limite++

			if limite == 10 {
				fmt.Printf("\n\t\t\t")
				limite = 0
			}
		}
	}
	fmt.Println(k)

	if acumulador == 2 {
		fmt.Println("Cantidad de divisores:", acumulador)
		fmt.Println("Si es primo")
		
	} else {
		fmt.Println("Cantidad de divisores:", acumulador)
		fmt.Println("No es primo")
	}
	
}